const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

///////////////



//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/' , express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_inscriere_daremania"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS inscriere(NumeEchipa VARCHAR(40),Jucator1 VARCHAR(40),Jucator2 VARCHAR(40),Jucator3 VARCHAR(40),Jucator4 VARCHAR(40),Jucator5 VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50))";
    connection.query(sql, function (err, result) {
        if (err) {throw err};
    });
});

app.post("/inscriere", (req, res) => {
    let NumeEchipa = req.body.NumeEchipa;
    let Jucator1 = req.body.Jucator1;
    let Jucator2 = req.body.Jucator2;
    let Jucator3 = req.body.Jucator3;
    let Jucator4 = req.body.Jucator4;
    let Jucator5 = req.body.Jucator5;
    let telefon = req.body.telefon;
    let email = req.body.email;
    
    let error = [];

         if(!NumeEchipa){
            console.log("Ati uitat sa completati numele echipei!");
            error.push("Ati uitat sa completati numele echipei");
        }
        if(NumeEchipa.length < 3 || NumeEchipa.length > 20){
            console.log("Nume invalid,sorry man!");
            error.push("Nume invalid,sorry man!");
        }
        else if(!NumeEchipa.match("^[A-Za-z]+$")) {
            console.log("Numele trebuie sa contina doar litere!");
            error.push("Numele trebuie sa contina doar litere!");
        }
    
        if(!Jucator1){
            console.log("Ati uitat sa completati numele jucatorului 1!");
            error.push("Ati uitat sa completati numele jucatorului 1");
        }
        if(Jucator1.length < 3 || Jucator1.length > 20){
            console.log("Nume invalid,sorry man!");
            error.push("Nume invalid,sorry man!");
        }
        
        if(!Jucator2){
          console.log("Ati uitat sa completati numele jucatorului 2!");
          error.push("Ati uitat sa completati numele jucatorului 2");
      }
      if(Jucator2.length < 3 || Jucator2.length > 20){
          console.log("Nume invalid,sorry man!");
          error.push("Nume invalid,sorry man!");
      }
      
      if(!Jucator3){
        console.log("Ati uitat sa completati numele jucatorului 3!");
        error.push("Ati uitat sa completati numele jucatorului 3");
      }
      if(Jucator3.length < 3 || Jucator3.length > 20){
        console.log("Nume invalid,sorry man!");
        error.push("Nume invalid,sorry man!");
      }
      
      
      if(!Jucator4){
      console.log("Ati uitat sa completati numele jucatorului 4!");
      error.push("Ati uitat sa completati numele jucatorului 4");
      }
      if(Jucator4.length < 3 || Jucator4.length > 20){
      console.log("Nume invalid,sorry man!");
      error.push("Nume invalid,sorry man!");
      }
     
     if(!Jucator5){
    console.log("Ati uitat sa completati numele jucatorului 5");
    error.push("Ati uitat sa completati numele jucatorului 5");
     }
     if(Jucator5.length < 3 || Jucator5.length > 20){
    console.log("Nume invalid,sorry man!");
    error.push("Nume invalid,sorry man!");
     }
     
        if(!telefon){
            console.log("Ati uitat sa completati cu nr dvs de telefon!");
            error.push("Ati uitat sa completati cu nr dcs de telefon");
        }
        if (telefon.length != 10) {
            console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
            error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
          } else if (!telefon.match("^[0-9]+$")) {
            console.log("Numarul de telefon trebuie sa contina doar cifre!");
            error.push("Numarul de telefon trebuie sa contina doar cifre!");
          }
          if(!email){
            console.log("Ati uitat sa completati tabela de email!");
            error.push("Ati uitat sa completati tabela de email");
        }
        if (!email.includes("@gmail.com") && !email.includes("@yahoo.com")) {
            console.log("Email invalid!");
            error.push("Email invalid!");
          }
          
          
      




    if (error.length === 0) {
        const sql =
            "INSERT INTO inscriere (NumeEchipa,Jucator1,Jucator2,Jucator3,Jucator4,Jucator5,telefon,email) VALUES('" +NumeEchipa +"','" +Jucator1 +"','" +Jucator2 +"','" +Jucator3 +"','" +Jucator4 +"','" + Jucator5 +"','" + telefon +"','" +email+"')";
        connection.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Inscrierea a avut loc cu succes!");
        });

        res.status(200).send({
            message: "Inscrierea a avut loc cu succes!!"
        });
        console.log(sql);
    } else {
        res.status(500).send(error)
        console.log("Eroare la inserarea in baza de date!");
    }
});