$(document).ready(function () {
    $('select').material_select();
});

document.querySelector('#regbutton').addEventListener("click", (e) => {
    e.preventDefault();
    toastr.remove()
    const inscriere = {
        NumeEchipa: document.querySelector('#NumeEchipa').value,
        Jucator1: document.querySelector('#Jucator1').value,
        Jucator2: document.querySelector('#Jucator2').value,
        Jucator3: document.querySelector('#Jucator3').value,
        Jucator4: document.querySelector('#Jucator4').value,
        Jucator5: document.querySelector('#Jucator5').value,
        email: document.querySelector('#email').value,
        telefon: document.querySelector('#telefon').value,
       
    }
    axios.post('/inscriere', inscriere)
        .then((response) => {
            toastr.success("Inscrierea a avut loc cu succes!");
        })
        .catch((error) => {
            const values = Object.values(error.response.data)
            console.log(error);
            values.map(item => {
                toastr.error(item)
            })
        })

}, false)